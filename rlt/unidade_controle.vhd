-------------------------------------------------------------
-- UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
-- INSTITUTO DE INFORMATICA
-- MAT�RIA         : MIC 59 
--                      PROF(A) FERNANDA L.K. e MARCELO S.L.
-- PROJETO         : CONSTRU��O DE UM PROCESSADOR NEANDER 
-- AUTORA          : CAROLINE MARTINS CONCATTO
-- FUNCAO          : UNIDADE DE CONTROLE
--                           
-- DATA DE CRIA��O : 30/03/2008
-- DEPEND�NCIAS    : 
----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
												  -- Bibliotecas --
------------------------------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_signed.all;

	
ENTITY unidade_controle is
	port(
		clk : in std_logic;
		rst	: in std_logic;
		
		nz_in		: in std_logic_vector(1 downto 0);
		ri_in		: in std_logic_vector(3 downto 0);
		write_read	: out std_logic_vector(1 downto 0); -- 10 = escrita 01 = leitura
		sel_ula		: out std_logic_vector(2 downto 0); -- 000 = soma / 001 = and / 010 = or / 011 = not / 100 = Y
		sel_rem		: out std_logic;
		enbl_rem	: out std_logic;
		enbl_rdm	: out std_logic;
		enblrdm_acc	: out std_logic;
		inc_pc		: out std_logic;
		enbl_ri		: out std_logic;
		enbl_acc	: out std_logic;
		enblacc_load: out std_logic; 	
		enbl_nz		: out std_logic;
		enbl_pc		: out std_logic;
		fim_out		: out std_logic
		
			
	);
end  unidade_controle;

architecture arch of unidade_controle is

-- simbolos da m�quina de estados
type estados_fsm is (
		t0,t1,t2,t3,t4,t5,t6,t7,fim
);
signal estado			: estados_fsm;
signal proximo_estado	: estados_fsm;

begin

--- registrador do estado (FSM)  
process (clk)
	begin
  	 if (clk='1' and clk'Event) then
 		if (rst ='1') then
    	 	estado<= t0;
    	else
      		estado <= proximo_estado; 
   		end if;
   	end if;
end process ; 

-- logica de proximo estado e saida 
process(estado, ri_in, nz_in)
	begin
		case estado is
			when t0 =>
       			proximo_estado <= t1;
			when t1 =>
       			proximo_estado <= t2;
       		when t2 =>
       			proximo_estado <= t3;
       		when t3 =>
       			if (ri_in = "0110" or  ri_in = "0000" or (ri_in = "1010" and nz_in(0) = '0') or (ri_in = "1001" and nz_in(1) = '0') ) then					-- NOT  or NOP or (JZ and Z=0) or ( JZ and Z=0)
       				proximo_estado <= t0;
       			elsif(ri_in = "1111") then
       				proximo_estado <= fim;
       			else 
       				proximo_estado <= t4;
       			end if;
       		when t4 =>
       			proximo_estado <= t5;
       		when t5 =>	
       			if(ri_in = "1000" or (ri_in = "1010" and nz_in(0) = '1') or (ri_in = "1001" and nz_in(1) = '1') ) then -- JUMP or (JZ and Z=1) or (JN and N=1)
       				proximo_estado <= t0;
       			else
       				proximo_estado <= t6;
       			end if;
       		when t6 =>
       			proximo_estado <= t7;
       		when t7 =>
       			proximo_estado <= t0;
       		when fim =>
       			proximo_estado <= t0;
       		when others =>
       			proximo_estado <= t0;		
       	end case;
end process; 	

process(estado,ri_in)
	begin
		case estado is
    	 	when t0 => 
    	 		fim_out <= '0';
    	 		write_read <= "00";	
    	 		enblrdm_acc <= '0';
    	 		enbl_pc <= '0';
    	 		enbl_rdm <='0';
    	 		inc_pc <= '0';
    	 		sel_ula	<= "111";
				enbl_acc <= '0';
				enbl_nz <= '0';
    	 		sel_rem <= '0';  
    	 		enbl_rem <= '1';      -- REM <= PC	
    	 	when t1 =>
				sel_rem <= '0';  
				enbl_rem <= '0';   
				write_read <= "01";
				inc_pc <= '1';
				--enbl_rdm <='1';
			when t2 =>
				sel_rem <= '0';  
				enbl_rem <= '0';   
				write_read <= "00";
				inc_pc <= '0';
				enbl_rdm <='1';
				enbl_ri <= '1';
			when t3 =>
				sel_rem <= '0';  
				enbl_rem <= '0';   
				write_read <= "00";
				inc_pc <= '0';
				enbl_rdm <='0';
				enbl_ri <= '0';
				if (ri_in = "0110") then	-- NOT
					enbl_acc <= '1';
					enbl_nz <= '1';
					sel_ula	<= "011";
				elsif((ri_in = "1010" and nz_in(0) = '0') or (ri_in = "1001" and nz_in(1) = '0') ) then	-- (JZ and Z=0) or (JN and N=0)
       				inc_pc <= '1';
       			else
					sel_rem <= '0';
					enbl_rem <= '1';  
					sel_ula	<= "111";
				end if;
			when t4 =>
				if (ri_in = "1000" or (ri_in = "1010" and nz_in(0) = '1') or (ri_in = "1001" and nz_in(1) = '1') ) then -- JUMP or (JZ and Z=1) or (JN and N=1)
					write_read <= "01";
				else
					write_read <= "01";
					inc_pc <= '1';
				end if;
				enbl_rem <= '0';
			when t5 =>
				enbl_rdm <='1';
				if (ri_in = "1000" or (ri_in = "1010" and nz_in(0) = '1') or (ri_in = "1001" and nz_in(1) = '1') ) then -- JUMP or (JZ and Z=1) or (JN and N=1)	
					enbl_pc <= '1';
				else
					enbl_rem <= '1';
					sel_rem <= '1';  
				end if;
				write_read <= "00";
				inc_pc <= '0';
			when t6 =>
				if (ri_in = "0001" ) then		-- STA
					enblrdm_acc <= '1';
				else
					write_read <= "01";
				end if;
				enbl_rem <= '0';	
				sel_rem <= '0'; 
				enbl_rdm <='0';
			when t7 =>
				if (ri_in = "0001") then			-- STA	
					write_read <= "10";	
					--enblrdm_acc <= '1';	
					enbl_rdm <='0';
				elsif (ri_in = "0010") then			--LOAD
					sel_ula <= "100"; 
					--enblrdm_acc <= '0';
					enbl_rdm <='1';
					write_read <= "00";
				elsif (ri_in = "0011") then			--ADD
					sel_ula <= "000";
					--enblrdm_acc <= '0';
					enbl_rdm <='1';
					write_read <= "00";
				elsif (ri_in = "0100") then			--OR
					sel_ula <= "010"; 
					--enblrdm_acc <= '0';
					enbl_rdm <='1';
					write_read <= "00";	 
				elsif (ri_in = "1010") then			--AND
					sel_ula <= "001"; 
					--enblrdm_acc <= '0';
					enbl_rdm <='1';
					write_read <= "00";	 	
				end if;
				    enblrdm_acc <= '0';
					enbl_acc <= '1';
					enbl_nz <= '1';
					enbl_pc <= '0';
				when others =>
				enblrdm_acc <= '0';
				enbl_pc <= '0';
				enbl_rdm <='0';
				enbl_pc <= '0';
				inc_pc <= '0';	
				sel_ula	<= "111";
				enbl_rdm <='0';
				enbl_acc <= '0';
				enbl_nz <= '0';
				sel_rem <= '0';
				enbl_rem <= '0'; 
				write_read <= "00";
				fim_out <= '1';	 
			end case;
end process;	

end arch;
	