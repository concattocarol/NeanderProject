-------------------------------------------------------------
-- UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
-- INSTITUTO DE INFORMATICA
-- MAT�RIA         : MIC 59 
--                      PROF(A) FERNANDA L.K. e MARCELO S.L.
-- PROJETO         : CONSTRU��O DE UM PROCESSADOR NEANDER 
-- AUTORA          : CAROLINE MARTINS CONCATTO
-- FUNCAO          : PACOTE DE TODAS AS PARTES DO NEANDER
--                           
-- DATA DE CRIA��O : 30/03/2008
-- DEPEND�NCIAS    : 
----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
												  -- Bibliotecas --
------------------------------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_signed.all;
-----------------------------------------------------------------------------------------------------------------------
													  -- PACOTES --
-----------------------------------------------------------------------------------------------------------------------
package neander_pkg is

component unidade_controle 
	port(
		clk 		: in std_logic;
		rst			: in std_logic;
		ri_in		: in std_logic_vector(3 downto 0);
		nz_in		: in std_logic_vector(1 downto 0);
		write_read	: out std_logic_vector(1 downto 0); 
		sel_ula		: out std_logic_vector(2 downto 0);
		sel_rem		: out std_logic;
		enbl_rem	: out std_logic;
		enbl_rdm	: out std_logic;
		enblrdm_acc	: out std_logic;
		inc_pc		: out std_logic;
		enbl_ri		: out std_logic;
		enbl_acc	: out std_logic;
		enbl_nz		: out std_logic;
		enbl_pc		: out std_logic;
		fim_out		: out std_logic
	);
end component;

component sequencial 
	port(
	      clk			: in std_logic;
	      rst			: in std_logic;
	      enbl_acc 		: in std_logic;
	      acc_in 		: in std_logic_vector(7 downto 0);
	      acc_out		: out std_logic_vector(7 downto 0);
	      enbl_rdm		: in std_logic;
	      enblrdm_acc	: in std_logic;
	      rdm_in 		: in std_logic_vector(7 downto 0);
	      rdm_out		: out std_logic_vector(7 downto 0);
	      enbl_rem		: in std_logic;
	      sel_rem		: in std_logic;
	      rem_out		: out std_logic_vector(7 downto 0);
	      enbl_nz		: in std_logic;
	      nz_in 		: in std_logic_vector(1 downto 0);
	      nz_out		: out std_logic_vector(1 downto 0);
	      enbl_pc		: in std_logic;
	      inc_pc		: in std_logic;
	      enbl_ri		: in std_logic;
	      ri_out		: out std_logic_vector(7 downto 0)
		);	
end component;

component  memoria IS
	port (
	   addr: IN std_logic_VECTOR(7 downto 0);
	   clk: IN std_logic;
	   din: IN std_logic_VECTOR(7 downto 0);
	   dout: OUT std_logic_VECTOR(7 downto 0);
	   we: IN std_logic);
end component;

component decod_instruction
	port( 
	  INSTRUCT	: in  std_logic_vector(3 downto 0);
	  MEMin		: in  std_logic_vector(7 downto 0);
	  ACin		: in  std_logic_vector(7 downto 0);
	  PCin		: in  std_logic_vector(3 downto 0);
	  ADDRin	: in  std_logic_vector(3 downto 0);
	  MEMout	: out std_logic_vector(7 downto 0);
	  ACout		: out std_logic_vector(7 downto 0);
	  Nout		: out std_logic;
	  Zout 		: out std_logic;
	  PCout		: out std_logic_vector(3 downto 0)
	);	    
end component;	

component ula5op_8bits 
	port( 
	   INX    : in  std_logic_vector(7 downto 0);
	   INY    : in  std_logic_vector(7 downto 0); 
	   SELULA : in  std_logic_vector(2 downto 0);
	   OUTULA : out std_logic_vector(7 downto 0); 
	   NEG    : out std_logic;
	   ZERO   : out std_logic  
	);
end component;	
end neander_pkg;