-------------------------------------------------------------
-- UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
-- INSTITUTO DE INFORMATICA
-- MAT�RIA         : MIC 59 
--                      PROF(A) FERNANDA L.K. e MARCELO S.L.
-- PROJETO         : CONSTRU��O DE UM PROCESSADOR NEANDER 
-- AUTORA          : CAROLINE MARTINS CONCATTO
-- FUNCAO          : Arquivo de teste do neander
--						Inicia o neander com reset e avisa quando 
--						terminou o programa com o sinal de FIM       
-- DATA DE CRIA��O : 25/04/2008
-- DEPEND�NCIAS    : 
----------------------------------------------------------------

entity neander_tb is
end neander_tb;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;            	
use unisim.vcomponents.all;	

architecture behav of neander_tb is

component neander  is
	port( 
	  clk : in std_logic;
	  rst : in std_logic;
	  
	  fim : out std_logic
	);
end component;
 signal clk, rst : std_logic := '1';
 signal fim		 : std_logic;
begin
clk <= not clk after 20 ns;


U1: neander 
	port map(
		clk => clk,
		rst => rst,
		fim => fim
	);

teste :process
begin
	-- reset the core
    assert false
      report "Resetando o core" & cr
      severity note;
    rst <= '1';
    wait for 500 ns;
    rst <= '0';
    if (fim = '1') then
    	  assert false
      report "Fim do programa" & cr
      severity note;
    end if;
  	wait;
end process;
	
	

end behav;	
