-------------------------------------------------------------
-- UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
-- INSTITUTO DE INFORMATICA
-- MAT�RIA         : MIC 59 
--                      PROF(A) FERNANDA L.K. e MARCELO S.L.
-- PROJETO         : CONSTRU��O DE UM PROCESSADOR NEANDER 
-- AUTORA          : CAROLINE MARTINS CONCATTO
-- FUNCAO          : Parte Sequencial
--						Cont�m todos os registradores do NEANDER
-- DATA DE CRIA��O : 30/03/2008
-- DEPEND�NCIAS    : 
----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
												  -- Bibliotecas --
------------------------------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_signed.all;

------------------------------------------------------------------------------------------------------------------------
												 	-- ENTIDADE --
------------------------------------------------------------------------------------------------------------------------

entity  sequencial is
	port(
	      clk			: in std_logic;
	      rst			: in std_logic;
	      enbl_acc 		: in std_logic;    						-- Habilita o acumulador
	      acc_in 		: in std_logic_vector(7 downto 0);      -- Valor de entrada do acc
	      acc_out		: out std_logic_vector(7 downto 0);     -- Valor de saida do acc
	      enbl_rdm		: in std_logic;							-- Habilita o registrador de dados vindos da mem�ria
	      enblrdm_acc	: in std_logic;                         -- Habilita o registrador de dados vindos da acc
	      rdm_in 		: in std_logic_vector(7 downto 0); 		-- Valor de entrada do RDM
	      rdm_out		: out std_logic_vector(7 downto 0);     -- Valor de saida do RDM
	      enbl_rem		: in std_logic;                    		-- Habilita o Regsitrador de endere�o
	      sel_rem		: in std_logic;                        	-- seleciona o dado de entrada do registrado de endere�o
	      rem_out		: out std_logic_vector(7 downto 0);     -- Valor de saida do REM
	      enbl_nz		: in std_logic;  					  	-- Habilita o registrado NZ
	      nz_in 		: in std_logic_vector(1 downto 0);      -- Valor de entrada do NZ
	      nz_out		: out std_logic_vector(1 downto 0);     -- Valor de saida do NZ  
	      enbl_pc		: in std_logic;                       	-- Habilita o PC
	      inc_pc		: in std_logic;                         -- Valor de entrada do pc
	      enbl_ri		: in std_logic;                         -- Habilita o Reg de Instru��es
	      ri_out		: out std_logic_vector(7 downto 0)     	-- Valor de saida do RI

	);
end sequencial;
-----------------------------------------------------------------------------------------------------------------------
													-- ARQUITETURA --
-----------------------------------------------------------------------------------------------------------------------
architecture arch of sequencial is
-----------------------------------------------------------------------------------------------------------------------
													-- Sinais --
-----------------------------------------------------------------------------------------------------------------------
signal rem_in	: std_logic_vector(7 downto 0);
signal rdm_int	: std_logic_vector(7 downto 0);
signal pc_int	: std_logic_vector(7 downto 0);
signal acc_int	: std_logic_vector(7 downto 0);

begin

-- REGISTRADOR DO ACC
process(clk)
variable var : std_logic_vector(7 downto 0);
	begin
		if clk = '1' and clk'event then
			if rst = '1' then
				var := (others => '0');
			elsif enbl_acc = '1' then
				var := acc_in;
			else
				var := var;
			end if;		
		end if;
		acc_int <= var;
end process;	

acc_out <= acc_int;
-- REGISTRADOR DO REM
process(clk)
variable var : std_logic_vector(7 downto 0);
	begin
		if clk = '1' and clk'event then
			if rst = '1' then
				var := (others => '1');
			elsif enbl_rem = '1' then
				var := rem_in;
			else
				var := var;
			end if;		
		end if;
		rem_out <= var;
end process;

with sel_rem select
	rem_in <= pc_int when '0',
	          rdm_int when others;


-- REGISTRADOR DO RDM
process(enbl_rdm)
variable var : std_logic_vector(7 downto 0);
	begin
		if enbl_rdm = '1' then
			var := rdm_in;
		elsif enblrdm_acc = '1' then
			var := acc_int;	
		else
			var := var;
		end if;		
		rdm_int <= var;
end process;

rdm_out <= rdm_int;

-- REGISTRADOR DO NZ
process(clk)
variable var : std_logic_vector(1 downto 0);
	begin
		if clk = '1' and clk'event then
			if rst = '1' then
				var := (others => '0');
			elsif enbl_nz = '1' then
				var := nz_in;
			else
				var := var;
			end if;		
		end if;
		nz_out <= var;
end process;

-- CONTADOR DO PC
process(clk)
variable cnt : std_logic_vector(7 downto 0);
	begin
		if clk = '1' and clk'event then
			if rst = '1' then
				cnt := (others => '0');
			elsif enbl_pc = '1' then
				cnt := rdm_int;
			elsif inc_pc = '1' then
				 cnt:= cnt + 1;
			end if;		
		end if;
		pc_int <= cnt;
end process;

-- REGISTRADOR DE INSTR�COES
process(clk)
variable var : std_logic_vector(7 downto 0);
begin
	if clk = '1' and clk'event then
		if rst = '1' then
			var := (others => '0');
		elsif enbl_ri= '1' then
			var := rdm_int;
		else
			var := var;
		end if;
	end if;	
	ri_out <= var;
end process;
end arch;