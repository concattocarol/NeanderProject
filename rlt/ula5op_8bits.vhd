-------------------------------------------------------------
-- UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
-- INSTITUTO DE INFORMATICA
-- MAT�RIA         : MIC 59 
--                      PROF(A) FERNANDA L.K. e MARCELO S.L.
-- PROJETO         : CONSTRU��O DE UM PROCESSADOR NEANDER 
-- AUTORA          : CAROLINE MARTINS CONCATTO
-- FUNCAO          : ULA DE 5 OPERACOES COM PALAVRAS DO TAMANHO DE 8 BITS
--                    SEL_ULA :
--					    soma   = "000"
--                      and    = "001"
--                      or     = "010"
--                      not    = "011"
--                      y      = "100"         
-- DATA DE CRIA��O : 30/03/2008
-- DEPEND�NCIAS    : 
----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
												  -- Bibliotecas --
------------------------------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_signed.all;

------------------------------------------------------------------------------------------------------------------------
												 	-- ENTIDADE --
------------------------------------------------------------------------------------------------------------------------

entity  ula5op_8bits is
	port( 
	   INX    : in  std_logic_vector(7 downto 0);		-- Entrada do dado da ULA
	   INY    : in  std_logic_vector(7 downto 0);       -- Entrada do dado da ULA
	   SELULA : in  std_logic_vector(2 downto 0);       -- Seleciona a opera��o que a ULA deve realizar
	   OUTULA : out std_logic_vector(7 downto 0);   	-- Sa�da da opera��o da ULA
	   NEG    : out std_logic;                          -- Estatus de nega��o da ULA  |NEG = 0 <=> OUTULA(7)= 0  | NEG = 1 <=> OUTULA(7)= 1                     	
	   ZERO   : out std_logic                           -- Estatus de nega��o da ULA  |ZERO = 0 <=> OUTULA!= 0  | ZERO = 1 <=> OUTULA= 0     
	);
end ula5op_8bits;
-----------------------------------------------------------------------------------------------------------------------
													-- ARQUITETURA --
-----------------------------------------------------------------------------------------------------------------------
architecture arch of ula5op_8bits is
-----------------------------------------------------------------------------------------------------------------------
													-- Sinais --
-----------------------------------------------------------------------------------------------------------------------
signal result : std_logic_vector(7 downto 0);

begin
  
  with selula select
           result <= inx + iny when "000",
                     inx and iny when "001",
                     inx or iny when "010",
                     not inx when "011",
                     iny when "100",
                     result when others;


-- sinaliza��o dos sinais de saida
NEG		<= '1' when result(7) = '1' else '0';
ZERO	<= '1' when result = "00000000" else '0';
OUTULA	<= result; 
end arch;