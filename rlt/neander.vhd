-------------------------------------------------------------
-- UNIVERSIDADE FEDERAL DO RIO GRANDE DO SUL
-- INSTITUTO DE INFORMATICA
-- MAT�RIA         : MIC 59 
--                      PROF(A) FERNANDA L.K. e MARCELO S.L.
-- PROJETO         : CONSTRU��O DE UM PROCESSADOR NEANDER 
-- AUTORA          : CAROLINE MARTINS CONCATTO
-- FUNCAO          : NEANDER YOPO
--                           
-- DATA DE CRIA��O : 30/03/2008
-- DEPEND�NCIAS    : 
----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
												  -- Bibliotecas --
------------------------------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_arith.all;
	use ieee.std_logic_signed.all;

use work.neander_pkg.all;
------------------------------------------------------------------------------------------------------------------------
												 	-- ENTIDADE --
------------------------------------------------------------------------------------------------------------------------

entity  neander  is
	port( 
	  clk : in std_logic;
	  rst : in std_logic;
	  
	  fim : out std_logic
	  
	);
end neander;
-----------------------------------------------------------------------------------------------------------------------
													-- ARQUITETURA --
-----------------------------------------------------------------------------------------------------------------------
architecture arch of neander is
-----------------------------------------------------------------------------------------------------------------------
													-- Sinais --
-----------------------------------------------------------------------------------------------------------------------
signal write_read_map : std_logic_vector(1 downto 0);
signal enblrdm_acc_map	: std_logic;
signal sel_rem_map 	: std_logic;
signal enbl_rem_map : std_logic;
signal enbl_rdm_map	: std_logic;
signal inc_pc_map	: std_logic;
signal enbl_ri_map	: std_logic;
signal enbl_acc_map	: std_logic;
signal enbl_nz_map	: std_logic;
signal enbl_pc_map	: std_logic;
signal dado_in_map	: std_logic_vector(7 downto 0);
signal dado_out_map	: std_logic_vector(7 downto 0);
signal rem_out_map	: std_logic_vector(7 downto 0);
signal ri_out_map   : std_logic_vector(7 downto 0);
signal nz_map		: std_logic_vector(1 downto 0);
signal acc_in_map	: std_logic_vector(7 downto 0);
signal sel_ula_map	: std_logic_vector(2 downto 0);
signal nz_out_map	: std_logic_vector(1 downto 0);
signal acc_out_map	: std_logic_vector(7 downto 0);

begin


NEANDER_UC : unidade_controle 
	port map(
		clk 		=> clk,
		rst			=> rst,
		ri_in		=> ri_out_map(3 downto 0),
		nz_in		=> nz_out_map,
		write_read	=> write_read_map,
		sel_ula		=> sel_ula_map,
		sel_rem		=> sel_rem_map,
		enbl_rem	=> enbl_rem_map,
		enbl_rdm	=> enbl_rdm_map,
		enblrdm_acc	=> enblrdm_acc_map,
		inc_pc		=> inc_pc_map,
		enbl_ri		=> enbl_ri_map,
		enbl_acc	=> enbl_acc_map,
		enbl_nz		=> enbl_nz_map,
		enbl_pc		=> enbl_pc_map,
		fim_out		=> fim
	);


NEANDER_REG: sequencial 
	port map(
	      clk			=> clk,
	      rst			=> rst,
	      enbl_acc 		=> enbl_acc_map,
	      acc_in 		=> acc_in_map,
	      acc_out		=> acc_out_map,
	      enbl_rdm		=> enbl_rdm_map,
	      enblrdm_acc	=> enblrdm_acc_map,
	      rdm_in 		=> dado_in_map,
	      rdm_out		=> dado_out_map,
	      enbl_rem		=> enbl_rem_map,
	      sel_rem		=> sel_rem_map,
	      rem_out		=> rem_out_map,
	      enbl_nz		=> enbl_nz_map,
	      nz_in 		=> nz_map,
	      nz_out		=> nz_out_map,
	      enbl_pc		=> enbl_pc_map,
	      inc_pc		=> inc_pc_map,
	      enbl_ri		=> enbl_ri_map,
	      ri_out		=> ri_out_map
		);	


NEANDER_MEM: memoria 
	port map(
	       addr =>rem_out_map,
	       clk  => clk,
	       din=> dado_out_map,
	       dout	=> dado_in_map,
	       we	=> write_read_map(1)
	     );

NEANDER_ULA : ula5op_8bits 
	port map( 
	   INX    => acc_out_map,
	   INY    => dado_out_map,
	   SELULA => sel_ula_map,
	   OUTULA => acc_in_map,
	   NEG    =>nz_map(1),
	   ZERO   =>nz_map(0)
	);   
	
	
end arch;