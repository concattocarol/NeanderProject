onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /neander_tb/clk
add wave -noupdate -format Logic /neander_tb/rst
add wave -noupdate -divider -height 30 {Unidade Controle}
add wave -noupdate -format Literal /neander_tb/u1/neander_uc/proximo_estado
add wave -noupdate -format Logic /neander_tb/u1/neander_uc/enbl_rdm
add wave -noupdate -format Logic /neander_tb/u1/neander_uc/rst
add wave -noupdate -format Literal /neander_tb/u1/neander_uc/estado
add wave -noupdate -format Literal /neander_tb/u1/neander_uc/ri_in
add wave -noupdate -format Logic /neander_tb/u1/neander_uc/sel_rem
add wave -noupdate -format Literal /neander_tb/u1/neander_uc/write_read
add wave -noupdate -format Logic /neander_tb/u1/neander_uc/inc_pc
add wave -noupdate -format Logic /neander_tb/u1/neander_uc/enbl_ri
add wave -noupdate -format Literal /neander_tb/u1/neander_uc/sel_ula
add wave -noupdate -format Logic /neander_tb/u1/neander_uc/enbl_rem
add wave -noupdate -divider -height 30 REGISTRADORES
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/pc_int
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/rem_out
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enbl_rdm
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/rdm_in
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enblrdm_acc
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/rdm_int
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/rdm_out
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enbl_ri
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/ri_out
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/acc_in
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/acc_out
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/clk
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enbl_acc
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enbl_nz
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enbl_pc
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/enbl_rem
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/inc_pc
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/nz_in
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/nz_out
add wave -noupdate -format Literal /neander_tb/u1/neander_reg/rem_in
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/rst
add wave -noupdate -format Logic /neander_tb/u1/neander_reg/sel_rem
add wave -noupdate -divider -height 30 ULA
add wave -noupdate -format Literal /neander_tb/u1/neander_ula/inx
add wave -noupdate -format Literal /neander_tb/u1/neander_ula/iny
add wave -noupdate -format Literal /neander_tb/u1/neander_ula/selula
add wave -noupdate -format Literal /neander_tb/u1/neander_ula/outula
add wave -noupdate -format Logic /neander_tb/u1/neander_ula/neg
add wave -noupdate -format Logic /neander_tb/u1/neander_ula/zero
add wave -noupdate -format Literal /neander_tb/u1/neander_ula/result
add wave -noupdate -divider -height 30 MEMORIA
add wave -noupdate -format Logic /neander_tb/u1/neander_mem/clk
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/write_read
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/rem_in
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/rdm_in
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/rdm_out
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_0
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_1
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_2
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_3
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_4
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_5
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_6
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_7
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_8
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_9
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_a
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_b
add wave -noupdate -format Literal /neander_tb/u1/neander_mem/byte_0_address_f
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 5} {157804000 ps} 0} {{Cursor 4} {7139758 ps} 0}
configure wave -namecolwidth 310
configure wave -valuecolwidth 61
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {6418624 ps} {7597438 ps}
bookmark add wave {Slot Time} {{21661885 ps} {43041085 ps}} 44
